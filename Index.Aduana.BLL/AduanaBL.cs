﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class AduanaBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public AduanaBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

            private int Correlativo()
            {
                int Id = 0;

                try
                {

                    Entities.Aduana AduanaActual = db.Set<Entities.Aduana>().Where(x => x.Fecha.Year == DateTime.Today.Year && x.Fecha.Month == DateTime.Today.Month && x.Fecha.Day == DateTime.Today.Day).OrderByDescending(x => x.Correlativo).FirstOrDefault();
                    int Inicial_Id = 1;

                    if (AduanaActual != null)
                    {
                        Inicial_Id = AduanaActual.Correlativo + 1;
                    }

                    Id = Inicial_Id;

                }
                catch (Exception)
                {
                }

                return Id;
            }

            private bool Agregar(Entities.Aduana entidad)
            {
                bool AduanaAgregar = false;

                try
                {
                     int Id = Correlativo();

                     if (Id > 0)
                     {                        
                         long lngAduanaId = new Herramienta().Formato_Correlativo(Id);

                         entidad.AduanaId = lngAduanaId;
                         entidad.Correlativo = Id;
                         entidad.Fecha = DateTime.Today;

                         db.Set<Entities.Aduana>().Add(entidad);
                         db.SaveChanges();

                         AduanaAgregar = true;
                     }                    
                }
                catch (Exception)
                {
                }

                return AduanaAgregar;
            }

            private bool Actualizar(Entities.Aduana entidad)
            {
                bool AduanaActualizar = false;

                try
                {

                    Entities.Aduana AduanaActual = ObtenerPorId(entidad.AduanaId);

                    if (AduanaActual.AduanaId > 0)
                    {
                        AduanaActual.PaisId = entidad.PaisId;
                        AduanaActual.Nombre = entidad.Nombre;
                        AduanaActual.Direccion = entidad.Direccion;

                        db.SaveChanges();
                        AduanaActualizar = true;
                    }

                }
                catch (Exception)
                {
                }

                return AduanaActualizar;
            }

        #endregion

        #region Metodos Publicos

            public string Guardar(Entities.Aduana entidad)
            {
                string Mensaje = "OK";
                bool OperacionExitosa = false;

                if (entidad.AduanaId > 0)
                {
                    OperacionExitosa = Actualizar(entidad);
                }
                else
                {
                    OperacionExitosa = Agregar(entidad);
                }

                if (!OperacionExitosa)
                {
                    Mensaje = "La información ingresada no es valida";
                }

                return Mensaje;
            }

            public Entities.Aduana ObtenerPorId(long id)
            {
                Entities.Aduana AduanaActual = new Entities.Aduana();

                try
                {
                    AduanaActual = db.Set<Entities.Aduana>().Include("Pais").Where(x => x.AduanaId == id).FirstOrDefault();
                }
                catch (Exception)
                {
                }

                return AduanaActual;
            }

            public List<Entities.Aduana> ObtenerListado()
            {
                List<Entities.Aduana> Aduanas = new List<Entities.Aduana>();

                try
                {
                    Aduanas = db.Set<Entities.Aduana>().Include("Pais").OrderByDescending(x => x.Fecha).ThenByDescending(x => x.AduanaId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return Aduanas;
            }

            public List<Entities.Aduana> Buscar(string Buscar)
            {
                List<Entities.Aduana> Aduanas = new List<Entities.Aduana>();

                try
                {
                    Aduanas = db.Set<Entities.Aduana>().Include("Pais").Where(x => x.Nombre.Contains(Buscar) || x.Direccion.Contains(Buscar)).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.AduanaId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return Aduanas;
            }

        #endregion

    }
}
