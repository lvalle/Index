﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class BodegaBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public BodegaBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

            private int Correlativo()
            {
                int Id = 0;

                try
                {

                    Bodega BodegaActual = db.Set<Bodega>().Where(x => x.Fecha.Year == DateTime.Today.Year && x.Fecha.Month == DateTime.Today.Month && x.Fecha.Day == DateTime.Today.Day).OrderByDescending(x => x.Correlativo).FirstOrDefault();
                    int Inicial_Id = 1;

                    if (BodegaActual != null)
                    {
                        Inicial_Id = BodegaActual.Correlativo + 1;
                    }

                    Id = Inicial_Id;

                }
                catch (Exception)
                {
                }

                return Id;
            }

            private bool Agregar(Bodega entidad)
            {
                bool BodegaAgregar = false;

                try
                {
                     int Id = Correlativo();

                     if (Id > 0)
                     {
                         long lngBodegaId = new Herramienta().Formato_Correlativo(Id);

                         entidad.BodegaId = lngBodegaId;
                         entidad.Correlativo = Id;
                         entidad.Fecha = DateTime.Today;

                         db.Set<Bodega>().Add(entidad);
                         db.SaveChanges();

                         BodegaAgregar = true;
                     }                    
                }
                catch (Exception)
                {
                }

                return BodegaAgregar;
            }

            private bool Actualizar(Bodega entidad)
            {
                bool BodegaActualizar = false;

                try
                {

                    Bodega BodegaActual = ObtenerPorId(entidad.BodegaId);

                    if (BodegaActual.BodegaId > 0)
                    {
                        BodegaActual.Nombre = entidad.Nombre;
                        BodegaActual.Direccion = entidad.Direccion;

                        db.SaveChanges();
                        BodegaActualizar = true;
                    }

                }
                catch (Exception)
                {
                }

                return BodegaActualizar;
            }

        #endregion

        #region Metodos Publicos

            public string Guardar(Bodega entidad)
            {
                string Mensaje = "OK";
                bool OperacionExitosa = false;

                if (entidad.BodegaId > 0)
                {
                    OperacionExitosa = Actualizar(entidad);
                }
                else
                {
                    OperacionExitosa = Agregar(entidad);
                }

                if (!OperacionExitosa)
                {
                    Mensaje = "La información ingresada no es valida";
                }

                return Mensaje;
            }

            public Bodega ObtenerPorId(long id)
            {
                Bodega BodegaActual = new Bodega();

                try
                {
                    BodegaActual = db.Set<Bodega>().Where(x => x.BodegaId == id).FirstOrDefault();
                }
                catch (Exception)
                {
                }

                return BodegaActual;
            }

            public List<Bodega> ObtenerListado()
            {
                List<Bodega> Bodegas = new List<Bodega>();

                try
                {
                    Bodegas = db.Set<Bodega>().OrderByDescending(x => x.Fecha).ThenByDescending(x => x.BodegaId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return Bodegas;
            }

            public List<Bodega> Buscar(string Buscar)
            {
                List<Bodega> Bodegas = new List<Bodega>();

                try
                {
                    Bodegas = db.Set<Bodega>().Where(x => x.Nombre.Contains(Buscar) || x.Direccion.Contains(Buscar)).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.BodegaId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return Bodegas;
            }

        #endregion

    }
}
