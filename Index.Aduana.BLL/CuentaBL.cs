﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class CuentaBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public CuentaBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

            private int Correlativo()
            {
                int Id = 0;

                try
                {

                    Cuenta CuentaActual = db.Set<Cuenta>().Where(x => x.Fecha.Year == DateTime.Today.Year && x.Fecha.Month == DateTime.Today.Month && x.Fecha.Day == DateTime.Today.Day).OrderByDescending(x => x.Correlativo).FirstOrDefault();
                    int Inicial_Id = 1;

                    if (CuentaActual != null)
                    {
                        Inicial_Id = CuentaActual.Correlativo + 1;
                    }

                    Id = Inicial_Id;

                }
                catch (Exception)
                {
                }

                return Id;
            }

            private bool Agregar(Cuenta entidad)
            {
                bool CuentaAgregar = false;

                try
                {
                     int Id = Correlativo();

                     if (Id > 0)
                     {
                         long lngCuentaId = new Herramienta().Formato_Correlativo(Id);

                         entidad.CuentaId = lngCuentaId;
                         entidad.Correlativo = Id;
                         entidad.Fecha = DateTime.Today;

                         db.Set<Cuenta>().Add(entidad);
                         db.SaveChanges();

                         CuentaAgregar = true;
                     }                    
                }
                catch (Exception)
                {
                }

                return CuentaAgregar;
            }

            private bool Actualizar(Cuenta entidad)
            {
                bool CuentaActualizar = false;

                try
                {

                    Cuenta CuentaActual = ObtenerPorId(entidad.CuentaId);

                    if (CuentaActual.CuentaId > 0)
                    {
                        CuentaActual.Nombre = entidad.Nombre;
                        CuentaActual.Descripcion = entidad.Descripcion;                       

                        db.SaveChanges();
                        CuentaActualizar = true;
                    }

                }
                catch (Exception)
                {
                }

                return CuentaActualizar;
            }

        #endregion

        #region Metodos Publicos

            public string Guardar(Cuenta entidad)
            {
                string Mensaje = "OK";
                bool OperacionExitosa = false;

                if (entidad.CuentaId > 0)
                {
                    OperacionExitosa = Actualizar(entidad);
                }
                else
                {
                    OperacionExitosa = Agregar(entidad);
                }

                if (!OperacionExitosa)
                {
                    Mensaje = "La información ingresada no es valida";
                }

                return Mensaje;
            }

            public Cuenta ObtenerPorId(long id)
            {
                Cuenta CuentaActual = new Cuenta();

                try
                {
                    CuentaActual = db.Set<Cuenta>().Where(x => x.CuentaId == id).FirstOrDefault();
                }
                catch (Exception)
                {
                }

                return CuentaActual;
            }

            public List<Cuenta> ObtenerListado()
            {
                List<Cuenta> Cuentas = new List<Cuenta>();

                try
                {
                    Cuentas = db.Set<Cuenta>().OrderByDescending(x => x.Fecha).ThenByDescending(x => x.CuentaId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return Cuentas;
            }

            public List<Cuenta> Buscar(string Buscar)
            {
                List<Cuenta> Cuentas = new List<Cuenta>();

                try
                {
                    Cuentas = db.Set<Cuenta>().Where(x => x.Nombre.Contains(Buscar) || x.Descripcion.Contains(Buscar)).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.CuentaId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return Cuentas;
            }

            public List<Cuenta> ObtenerCuentaPorClienteId(long clienteId)
            {
                List<Cuenta> Cuentas = new List<Cuenta>();

                try
                {
                    Cuentas = db.Set<CuentaCliente>().Where(x => x.ClienteId == clienteId).Join(db.Set<Cuenta>(), CC => CC.CuentaId, C => C.CuentaId, (CC, C) => new { C }).Select(x => x.C).ToList();
                }
                catch (Exception)
                {
                }

                return Cuentas;
            }
        #endregion

    }
}
