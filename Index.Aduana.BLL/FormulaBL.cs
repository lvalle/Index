﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using Sistema.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class FormulaBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public FormulaBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados
         
            private int Correlativo()
            {
                int Id = 0;

                try
                {

                    Formula FormulaActual = db.Set<Formula>().Where(x => x.Fecha.Year == DateTime.Today.Year && x.Fecha.Month == DateTime.Today.Month && x.Fecha.Day == DateTime.Today.Day).OrderByDescending(x => x.Correlativo).FirstOrDefault();
                    int Inicial_Id = 1;

                    if (FormulaActual != null)
                    {
                        Inicial_Id = FormulaActual.Correlativo + 1;
                    }

                    Id = Inicial_Id;

                }
                catch (Exception)
                {
                }

                return Id;
            }

            private bool Agregar(Formula entidad)
            {
                bool FormulaAgregar = false;

                try
                {
                    int Id = Correlativo();

                    if (Id > 0)
                    {
                        long lngFormulaId = new Herramienta().Formato_Correlativo(Id);

                        if (lngFormulaId > 0)
                        {
                            entidad.FormulaId = lngFormulaId;
                            entidad.Correlativo = Id;
                            entidad.Fecha = DateTime.Today;

                            if (entidad.Detalles != null && entidad.Detalles.Count() > 0)
                            {
                                int DetalleId = 1; 
                                foreach (var Detalle in entidad.Detalles)
                                {
                                    Detalle.DetalleId = DetalleId;
                                    Detalle.FormulaId = entidad.FormulaId;
                                    DetalleId++;
                                }
                            }

                            db.Set<Formula>().Add(entidad);
                            db.SaveChanges();
                            FormulaAgregar = true;
                        }
                    }

                }
                catch (Exception)
                {
                }

                return FormulaAgregar;
            }

            private bool Actualizar(Formula entidad)
            {
                bool FormulaActualizar = false;

                try
                {

                    Formula FormulaActual = ObtenerPorId(entidad.FormulaId);

                    if (FormulaActual.FormulaId > 0)
                    {   
                        if (entidad.Detalles != null && entidad.Detalles.Count() > 0)
                        {
                            //Eliminar las materias por formulaId                          
                            var Detalles = db.Set<FormulaDetalle>().Where(x => x.FormulaId == FormulaActual.FormulaId).ToList();
                            db.Set<FormulaDetalle>().RemoveRange(Detalles);

                            //Agregar las nuevas formulas
                            FormulaActual.Detalles = new List<FormulaDetalle>();

                            int DetalleId = 1;
                            foreach (var Detalle in entidad.Detalles)
                            {
                                if (!FormulaActual.Detalles.Any(x => x.MateriaId == Detalle.MateriaId))
                                {
                                    FormulaActual.Detalles.Add(new FormulaDetalle() { DetalleId = DetalleId, FormulaId = FormulaActual.FormulaId, MateriaId = Detalle.MateriaId,  Cantidad = Detalle.Cantidad, CantidadMerma = Detalle.CantidadMerma });
                                    DetalleId++;
                                }
                            }

                        }                       

                        db.SaveChanges();
                        FormulaActualizar = true;
                    }

                }
                catch (Exception)
                {
                }

                return FormulaActualizar;
            }          

        #endregion

        #region Metodos Publicos           

            public string Guardar(Formula entidad)
            {
                string Mensaje = "OK";
                bool OperacionExitosa = false;

                if (entidad.FormulaId > 0)
                {
                    OperacionExitosa = Actualizar(entidad);
                }
                else
                {
                    OperacionExitosa = Agregar(entidad);
                }

                if (!OperacionExitosa)
                {
                    Mensaje = "La información ingresada no es valida";
                }

                return Mensaje;
            }

            public Formula ObtenerPorId(long id, bool todo = false)
            {
                Formula FormulaActual = new Formula();

                try
                {
                    if (todo)
                    {
                        FormulaActual = db.Set<Formula>().Include("Producto").Include("Detalles").Include("Detalles.Materia").Where(x => x.FormulaId == id).FirstOrDefault();
                    }
                    else
                    {
                        FormulaActual = db.Set<Formula>().Include("Producto").Where(x => x.FormulaId == id).FirstOrDefault();
                    }                    
                }
                catch (Exception)
                {
                }

                return FormulaActual;
            }

            public List<Formula> ObtenerListado(long clienteId)
            {
                List<Formula> Formulas = new List<Formula>();

                try
                {
                    Formulas = db.Set<Formula>().Include("Producto").Include("Detalles").Where(x => x.ClienteId == clienteId).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.FormulaId).ToList();
                }
                catch (Exception)
                {
                }

                return Formulas;
            }

            public List<Formula> Buscar(string Buscar, long clienteId)
            {
                List<Formula> Formulas = new List<Formula>();

                try
                {
                    Formulas = db.Set<Formula>().Include("Producto").Include("Detalles").Where(x => x.Producto.Nombre.Contains(Buscar) && x.ClienteId == clienteId).ToList();
                }
                catch (Exception)
                {
                }

                return Formulas;
            }

        #endregion

    }
}
