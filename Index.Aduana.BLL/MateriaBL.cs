﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class MateriaBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public MateriaBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

            private int Correlativo()
            {
                int Id = 0;

                try
                {

                    Materia MateriaActual = db.Set<Materia>().Where(x => x.Fecha.Year == DateTime.Today.Year && x.Fecha.Month == DateTime.Today.Month && x.Fecha.Day == DateTime.Today.Day).OrderByDescending(x => x.Correlativo).FirstOrDefault();
                    int Inicial_Id = 1;

                    if (MateriaActual != null)
                    {
                        Inicial_Id = MateriaActual.Correlativo + 1;
                    }

                    Id = Inicial_Id;

                }
                catch (Exception)
                {
                }

                return Id;
            }

            private bool Agregar(Materia entidad)
            {
                bool MateriaAgregar = false;

                try
                {
                     int Id = Correlativo();

                     if (Id > 0)
                     {
                         long lngMateriaId = new Herramienta().Formato_Correlativo(Id);

                         entidad.MateriaId = lngMateriaId;
                         entidad.Correlativo = Id;
                         entidad.Fecha = DateTime.Today;

                         db.Set<Materia>().Add(entidad);
                         db.SaveChanges();

                         MateriaAgregar = true;
                     }                    
                }
                catch (Exception)
                {
                }

                return MateriaAgregar;
            }

            private bool Actualizar(Materia entidad)
            {
                bool MateriaActualizar = false;

                try
                {

                    Materia MateriaActual = ObtenerPorId(entidad.MateriaId);

                    if (MateriaActual.MateriaId > 0)
                    {
                        MateriaActual.UnidadId = entidad.UnidadId;
                        MateriaActual.Codigo = entidad.Codigo;
                        MateriaActual.Nombre = entidad.Nombre;
                        MateriaActual.Descripcion = entidad.Descripcion;
                        MateriaActual.Barra = entidad.Barra;
                        MateriaActual.Partida = entidad.Partida;
                        MateriaActual.CuentaId = entidad.CuentaId;

                        db.SaveChanges();
                        MateriaActualizar = true;
                    }

                }
                catch (Exception)
                {
                }

                return MateriaActualizar;
            }

        #endregion

        #region Metodos Publicos

            public string Guardar(Materia entidad)
            {
                string Mensaje = "OK";
                bool OperacionExitosa = false;

                if (entidad.MateriaId > 0)
                {
                    OperacionExitosa = Actualizar(entidad);
                }
                else
                {
                    OperacionExitosa = Agregar(entidad);
                }

                if (!OperacionExitosa)
                {
                    Mensaje = "La información ingresada no es valida";
                }

                return Mensaje;
            }

            public Materia ObtenerPorId(long id)
            {
                Materia MateriaActual = new Materia();

                try
                {
                    MateriaActual = db.Set<Materia>().Where(x => x.MateriaId == id).FirstOrDefault();      
                }
                catch (Exception)
                {
                }

                return MateriaActual;
            }

            public List<Materia> ObtenerListado(long clienteId)
            {
                List<Materia> Materias = new List<Materia>();

                try
                {
                    Materias = db.Set<Materia>().Include("Unidad").Include("Cuenta").Where(x => x.ClienteId == clienteId).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.MateriaId).Take(200).ToList();             
                }
                catch (Exception)
                {
                }

                return Materias;
            }

            public List<Materia> Buscar(string Buscar, long clienteId)
            {
                List<Materia> Materias = new List<Materia>();

                try
                {
                    Materias = db.Set<Materia>().Include("Unidad").Include("Cuenta").Where(x => (x.ClienteId == clienteId) && (x.Codigo.Contains(Buscar) || x.Nombre.Contains(Buscar) || x.Barra.Contains(Buscar) || x.Partida.Contains(Buscar))).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.MateriaId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return Materias;
            }

        #endregion

    }
}
