﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class MonedaBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public MonedaBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

        #endregion

        #region Metodos Publicos

            public List<Moneda> ObtenerListado()
            {
                List<Moneda> Monedas = new List<Moneda>();

                try
                {
                    Monedas = db.Set<Moneda>().ToList();
                }
                catch (Exception)
                {
                }

                return Monedas;
            }

        #endregion

    }
}
