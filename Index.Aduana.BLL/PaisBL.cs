﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class PaisBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public PaisBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

            private int Correlativo()
            {
                int Id = 0;

                try
                {

                    Pais PaisActual = db.Set<Pais>().Where(x => x.Fecha.Year == DateTime.Today.Year && x.Fecha.Month == DateTime.Today.Month && x.Fecha.Day == DateTime.Today.Day).OrderByDescending(x => x.Correlativo).FirstOrDefault();
                    int Inicial_Id = 1;

                    if (PaisActual != null)
                    {
                        Inicial_Id = PaisActual.Correlativo + 1;
                    }

                    Id = Inicial_Id;

                }
                catch (Exception)
                {
                }

                return Id;
            }

            private bool Agregar(Pais entidad)
            {
                bool PaisAgregar = false;

                try
                {
                     int Id = Correlativo();

                     if (Id > 0)
                     {                        
                         long lngPaisId = new Herramienta().Formato_Correlativo(Id);

                         entidad.PaisId = lngPaisId;
                         entidad.Correlativo = Id;
                         entidad.Fecha = DateTime.Today;

                         db.Set<Pais>().Add(entidad);
                         db.SaveChanges();

                         PaisAgregar = true;
                     }                    
                }
                catch (Exception)
                {
                }

                return PaisAgregar;
            }

            private bool Actualizar(Pais entidad)
            {
                bool PaisActualizar = false;

                try
                {

                    Pais PaisActual = ObtenerPorId(entidad.PaisId);

                    if (PaisActual.PaisId > 0)
                    {
                        PaisActual.Nombre = entidad.Nombre;                       

                        db.SaveChanges();
                        PaisActualizar = true;
                    }

                }
                catch (Exception)
                {
                }

                return PaisActualizar;
            }

        #endregion

        #region Metodos Publicos

            public string Guardar(Pais entidad)
            {
                string Mensaje = "OK";
                bool OperacionExitosa = false;

                if (entidad.PaisId > 0)
                {
                    OperacionExitosa = Actualizar(entidad);
                }
                else
                {
                    OperacionExitosa = Agregar(entidad);
                }

                if (!OperacionExitosa)
                {
                    Mensaje = "La información ingresada no es valida";
                }

                return Mensaje;
            }

            public Pais ObtenerPorId(long id)
            {
                Pais PaisActual = new Pais();

                try
                {
                    PaisActual = db.Set<Pais>().Where(x => x.PaisId == id).FirstOrDefault();
                }
                catch (Exception)
                {
                }

                return PaisActual;
            }

            public List<Pais> ObtenerListado(bool todos = true)
            {
                List<Pais> Paises = new List<Pais>();

                try
                {
                    if (todos)
                    {
                        Paises = db.Set<Pais>().OrderByDescending(x => x.Fecha).ThenByDescending(x => x.PaisId).Take(200).ToList();
                    }
                    else
                    {
                        Paises = db.Set<Pais>().Where(x => x.PaisPadreId == null).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.PaisId).ToList();                  
                    }                    
                }
                catch (Exception)
                {
                }

                return Paises;
            }

            public List<Pais> Buscar(string Buscar)
            {
                List<Pais> Paises = new List<Pais>();

                try
                {
                    Paises = db.Set<Pais>().Where(x => x.Nombre.Contains(Buscar)).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.PaisId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return Paises;
            }

        #endregion

    }
}
