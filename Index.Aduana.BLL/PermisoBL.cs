﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class PermisoBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public PermisoBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

        #endregion

        #region Metodos Publicos

            public List<Permiso> ObtenerListado()
            {
                List<Permiso> Permisos = new List<Permiso>();

                try
                {
                    Permisos = db.Set<Permiso>().ToList();
                }
                catch (Exception)
                {
                }

                return Permisos;
            }

        #endregion

    }
}
