﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class PersonaBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public PersonaBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

            private int Correlativo()
            {
                int Id = 0;

                try
                {

                    Persona PersonaActual = db.Set<Persona>().Where(x => x.Fecha.Year == DateTime.Today.Year && x.Fecha.Month == DateTime.Today.Month && x.Fecha.Day == DateTime.Today.Day).OrderByDescending(x => x.Correlativo).FirstOrDefault();
                    int Inicial_Id = 1;

                    if (PersonaActual != null)
                    {
                        Inicial_Id = PersonaActual.Correlativo + 1;
                    }

                    Id = Inicial_Id;

                }
                catch (Exception)
                {
                }

                return Id;
            }

            private bool Agregar(Persona entidad)
            {
                bool PersonaAgregar = false;

                try
                {
                     int Id = Correlativo();

                     if (Id > 0)
                     {
                         long lngPersonaId = new Herramienta().Formato_Correlativo(Id);

                         entidad.PersonaId = lngPersonaId;
                         entidad.Correlativo = Id;
                         entidad.Fecha = DateTime.Today;

                         if (entidad.Telefonos != null && entidad.Telefonos.Count() > 0)
                         {
                             int TelefonoId = 1;
                             foreach (var Telefono in entidad.Telefonos)
                             {
                                 Telefono.TelefonoId = TelefonoId;
                                 Telefono.PersonaId = entidad.PersonaId;
                                 TelefonoId++;
                             }                             
                         }

                         if (entidad.Cuentas != null && entidad.Cuentas.Count() > 0)
                         {
                             foreach (var Cuenta in entidad.Cuentas)
                             {
                                 Cuenta.ClienteId = entidad.PersonaId;                                 
                             }                          
                         }

                         db.Set<Persona>().Add(entidad);
                         db.SaveChanges();

                         PersonaAgregar = true;
                     }                    
                }
                catch (Exception)
                {
                }

                return PersonaAgregar;
            }

            private bool Actualizar(Persona entidad)
            {
                bool PersonaActualizar = false;

                try
                {

                    Persona PersonaActual = ObtenerPorId(entidad.PersonaId);

                    if (PersonaActual.PersonaId > 0)
                    {
                        PersonaActual.Nit = entidad.Nit;
                        PersonaActual.Nombre = entidad.Nombre;
                        PersonaActual.Direccion = entidad.Direccion;
                        PersonaActual.RepresentanteLegal = entidad.RepresentanteLegal;
                        PersonaActual.Codigo = entidad.Codigo;
                        PersonaActual.CodigoImportador = entidad.CodigoImportador;
                        PersonaActual.CodigoExportador = entidad.CodigoExportador;
                        PersonaActual.ResolucionCalificacion = entidad.ResolucionCalificacion;
                        PersonaActual.RegimenCalificacion = entidad.RegimenCalificacion;
                        PersonaActual.PeriodoFiscal = entidad.PeriodoFiscal;
                        PersonaActual.FechaResolucion = entidad.FechaResolucion;
                        PersonaActual.FechaVencimiento = entidad.FechaVencimiento;
                        PersonaActual.Observaciones = entidad.Observaciones;
                        PersonaActual.LocalExterno = entidad.LocalExterno;

                        if (entidad.Telefonos != null && entidad.Telefonos.Count() > 0)
                        {
                            //Eliminar por personaId                          
                            var Telefonos = db.Set<PersonaTelefono>().Where(x => x.PersonaId == PersonaActual.PersonaId).ToList();
                            db.Set<PersonaTelefono>().RemoveRange(Telefonos);

                            //Agregar los nuevos telefonos
                            PersonaActual.Telefonos = new List<PersonaTelefono>();

                            int TelefonoId = 1;
                            foreach (var Telefono in entidad.Telefonos)
                            {
                                PersonaActual.Telefonos.Add(new PersonaTelefono() { TelefonoId = TelefonoId, TipoId = Telefono.TipoId, PersonaId = PersonaActual.PersonaId, Numero = Telefono.Numero });
                                TelefonoId++;
                            }

                        }

                        if (entidad.Cuentas != null && entidad.Cuentas.Count() > 0)
                        {
                            //Eliminar por personaId                          
                            var Cuentas = db.Set<CuentaCliente>().Where(x => x.ClienteId == PersonaActual.PersonaId).ToList();
                            db.Set<CuentaCliente>().RemoveRange(Cuentas);

                            //Agregar las nuevas cuentas
                            PersonaActual.Cuentas = new List<CuentaCliente>();

                            foreach (var Cuenta in entidad.Cuentas)
                            {                               
                                PersonaActual.Cuentas.Add(new CuentaCliente() { CuentaId = Cuenta.CuentaId, ClienteId = entidad.PersonaId });
                            }
                        }

                        db.SaveChanges();
                        PersonaActualizar = true;
                    }

                }
                catch (Exception)
                {
                }

                return PersonaActualizar;
            }

        #endregion

        #region Metodos Publicos

            public string Guardar(Persona entidad)
            {
                string Mensaje = "OK";
                bool OperacionExitosa = false;

                if (entidad.PersonaId > 0)
                {
                    OperacionExitosa = Actualizar(entidad);
                }
                else
                {
                    OperacionExitosa = Agregar(entidad);
                }

                if (!OperacionExitosa)
                {
                    Mensaje = "La información ingresada no es valida";
                }

                return Mensaje;
            }

            public Persona ObtenerPorId(long id, bool todo = false)
            {
                Persona PersonaActual = new Persona();

                try
                {
                    if (todo)
                    {
                        PersonaActual = db.Set<Persona>().Include("Telefonos").Include("Cuentas").Where(x => x.PersonaId == id).FirstOrDefault();
                    }
                    else
                    {
                        PersonaActual = db.Set<Persona>().Where(x => x.PersonaId == id).FirstOrDefault();
                    }                    
                }
                catch (Exception)
                {
                }

                return PersonaActual;
            }

            public List<Persona> ObtenerListado(int tipoId, bool todo = false)
            {
                List<Persona> Personas = new List<Persona>();

                try
                {
                    if (todo)
                    {
                        Personas = db.Set<Persona>().Where(x => x.TipoId == tipoId).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.PersonaId).ToList();
                    }
                    else
                    {
                        Personas = db.Set<Persona>().Where(x => x.TipoId == tipoId).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.PersonaId).Take(200).ToList();
                    }                    
                }
                catch (Exception)
                {
                }

                return Personas;
            }           

            public List<Persona> Buscar(string Buscar, int tipoId)
            {
                List<Persona> Personas = new List<Persona>();

                try
                {
                    Personas = db.Set<Persona>().Where(x => (x.TipoId == tipoId) && (x.Nit.Contains(Buscar) || x.Nombre.Contains(Buscar) || x.RepresentanteLegal.Contains(Buscar) || x.Codigo.Contains(Buscar) || x.CodigoImportador.Contains(Buscar) || x.CodigoExportador.Contains(Buscar) || x.ResolucionCalificacion.Contains(Buscar) || x.RegimenCalificacion.Contains(Buscar) || x.PeriodoFiscal.Contains(Buscar))).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.PersonaId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return Personas;
            }

            public List<Persona> ClientesPorUsuarioId(long usuarioId) 
            {
                List<Persona> Clientes = new List<Persona>();

                try
                {
                    Clientes = db.Set<UsuarioCliente>().Where(x => x.UsuarioId == usuarioId).Join(db.Set<Persona>().Where(x => x.TipoId == 1), UC => UC.ClienteId, C => C.PersonaId, (UC, C) => new { C }).Select(x => x.C).ToList();
                }
                catch (Exception)
                {
                }

                return Clientes;
            }

        #endregion

    }
}
