﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class PersonaTipoBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public PersonaTipoBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

        #endregion

        #region Metodos Publicos

            public List<PersonaTipo> ObtenerListado()
            {
                List<PersonaTipo> PersonaTipos = new List<PersonaTipo>();

                try
                {
                    PersonaTipos = db.Set<PersonaTipo>().ToList();
                }
                catch (Exception)
                {
                }

                return PersonaTipos;
            }

        #endregion

    }
}
