﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class ProductoBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public ProductoBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

            private int Correlativo()
            {
                int Id = 0;

                try
                {

                    Producto ProductoActual = db.Set<Producto>().Where(x => x.Fecha.Year == DateTime.Today.Year && x.Fecha.Month == DateTime.Today.Month && x.Fecha.Day == DateTime.Today.Day).OrderByDescending(x => x.Correlativo).FirstOrDefault();
                    int Inicial_Id = 1;

                    if (ProductoActual != null)
                    {
                        Inicial_Id = ProductoActual.Correlativo + 1;
                    }

                    Id = Inicial_Id;

                }
                catch (Exception)
                {
                }

                return Id;
            }

            private bool Agregar(Producto entidad)
            {
                bool ProductoAgregar = false;

                try
                {
                     int Id = Correlativo();

                     if (Id > 0)
                     {
                         long lngProductoId = new Herramienta().Formato_Correlativo(Id);

                         entidad.ProductoId = lngProductoId;
                         entidad.Correlativo = Id;
                         entidad.Fecha = DateTime.Today;

                         if (entidad.Materias != null && entidad.Materias.Count() > 0)
                         {
                             foreach (var Materia in entidad.Materias)
                             {
                                 Materia.ProductoId = entidad.ProductoId;
                             }
                         }

                         db.Set<Producto>().Add(entidad);
                         db.SaveChanges();

                         ProductoAgregar = true;
                     }                    
                }
                catch (Exception)
                {
                }

                return ProductoAgregar;
            }

            private bool Actualizar(Producto entidad)
            {
                bool ProductoActualizar = false;

                try
                {

                    Producto ProductoActual = ObtenerPorId(entidad.ProductoId);

                    if (ProductoActual.ProductoId > 0)
                    {
                        ProductoActual.UnidadId = entidad.UnidadId;
                        ProductoActual.Codigo = entidad.Codigo;
                        ProductoActual.Nombre = entidad.Nombre;
                        ProductoActual.Descripcion = entidad.Descripcion;
                        ProductoActual.Barra = entidad.Barra;
                        ProductoActual.Partida = entidad.Partida;

                        if (entidad.Materias != null && entidad.Materias.Count() > 0)
                        {
                            //Eliminar las materias por productoId                          
                            var Detalles = db.Set<ProductoMateria>().Where(x => x.ProductoId == ProductoActual.ProductoId).ToList();
                            db.Set<ProductoMateria>().RemoveRange(Detalles);

                            //Agregar las nuevas materias
                            ProductoActual.Materias = new List<ProductoMateria>();

                            foreach (var Detalle in entidad.Materias)
                            {
                                ProductoActual.Materias.Add(new ProductoMateria() { MateriaId = Detalle.MateriaId, ProductoId = ProductoActual.ProductoId });
                            }
                        }

                        db.SaveChanges();
                        ProductoActualizar = true;
                    }

                }
                catch (Exception)
                {
                }

                return ProductoActualizar;
            }

        #endregion

        #region Metodos Publicos

            public string Guardar(Producto entidad)
            {
                string Mensaje = "OK";
                bool OperacionExitosa = false;

                if (entidad.ProductoId > 0)
                {
                    OperacionExitosa = Actualizar(entidad);
                }
                else
                {
                    OperacionExitosa = Agregar(entidad);
                }

                if (!OperacionExitosa)
                {
                    Mensaje = "La información ingresada no es valida";
                }

                return Mensaje;
            }

            public Producto ObtenerPorId(long id, bool todos = false)
            {
                Producto ProductoActual = new Producto();

                try
                {
                    if (todos)
                    {
                        ProductoActual = db.Set<Producto>().Include("Unidad").Include("Materias").Include("Materias.Materia").Where(x => x.ProductoId == id).FirstOrDefault();
                    }
                    else
                    {
                        ProductoActual = db.Set<Producto>().Include("Unidad").Where(x => x.ProductoId == id).FirstOrDefault();
                    }
                }
                catch (Exception)
                {
                }

                return ProductoActual;
            }

            public List<Producto> ObtenerListado(bool todos = true, long clienteId = 0)
            {
                List<Producto> Productos = new List<Producto>();

                try
                {
                    if (todos)
                    {
                      Productos = db.Set<Producto>().Include("Unidad").Where(x => x.ClienteId == clienteId).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.ProductoId).Take(200).ToList();                      
                    }
                    else
                    {
                        Productos = db.Set<Producto>().Where(x => x.ClienteId == clienteId).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.ProductoId).Take(200).ToList();                                             
                    }                    
                }
                catch (Exception)
                {
                }

                return Productos;
            }

            public List<Producto> Buscar(string Buscar, long clienteId)
            {
                List<Producto> Productos = new List<Producto>();

                try
                {
                    Productos = db.Set<Producto>().Include("Unidad").Where(x => (x.ClienteId == clienteId) && (x.Codigo.Contains(Buscar) || x.Nombre.Contains(Buscar) || x.Barra.Contains(Buscar) || x.Partida.Contains(Buscar))).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.ProductoId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return Productos;
            }

        #endregion

    }
}
