﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class TelefonoTipoBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public TelefonoTipoBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

        #endregion

        #region Metodos Publicos

            public List<TelefonoTipo> ObtenerListado()
            {
                List<TelefonoTipo> TelefonoTipos = new List<TelefonoTipo>();

                try
                {
                    TelefonoTipos = db.Set<TelefonoTipo>().ToList();
                }
                catch (Exception)
                {
                }

                return TelefonoTipos;
            }

        #endregion

    }
}
