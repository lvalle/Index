﻿using Index.Aduana.DAL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.BLL
{
    public class UnidadBL
    {

        #region Variables Globales

            private IndexContext db;

        #endregion

        #region Constructores

            public UnidadBL()
            {
                this.db = new IndexContext();
            }

        #endregion

        #region Metodos Privados

            private int Correlativo()
            {
                int Id = 0;

                try
                {

                    UnidadMedida UnidadMedidaActual = db.Set<UnidadMedida>().Where(x => x.Fecha.Year == DateTime.Today.Year && x.Fecha.Month == DateTime.Today.Month && x.Fecha.Day == DateTime.Today.Day).OrderByDescending(x => x.Correlativo).FirstOrDefault();
                    int Inicial_Id = 1;

                    if (UnidadMedidaActual != null)
                    {
                        Inicial_Id = UnidadMedidaActual.Correlativo + 1;
                    }

                    Id = Inicial_Id;

                }
                catch (Exception)
                {
                }

                return Id;
            }

            private bool Agregar(UnidadMedida entidad)
            {
                bool UnidadMedidaAgregar = false;

                try
                {
                     int Id = Correlativo();

                     if (Id > 0)
                     {
                         long lngUnidadMedidaId = new Herramienta().Formato_Correlativo(Id);

                         entidad.UnidadId = lngUnidadMedidaId;
                         entidad.Correlativo = Id;
                         entidad.Fecha = DateTime.Today;

                         db.Set<UnidadMedida>().Add(entidad);
                         db.SaveChanges();

                         UnidadMedidaAgregar = true;
                     }                    
                }
                catch (Exception)
                {
                }

                return UnidadMedidaAgregar;
            }

            private bool Actualizar(UnidadMedida entidad)
            {
                bool UnidadMedidaActualizar = false;

                try
                {

                    UnidadMedida UnidadMedidaActual = ObtenerPorId(entidad.UnidadId);

                    if (UnidadMedidaActual.UnidadId > 0)
                    {
                        UnidadMedidaActual.Nombre = entidad.Nombre;
                        UnidadMedidaActual.Descripcion = entidad.Descripcion;

                        db.SaveChanges();
                        UnidadMedidaActualizar = true;
                    }

                }
                catch (Exception)
                {
                }

                return UnidadMedidaActualizar;
            }

        #endregion

        #region Metodos Publicos

            public string Guardar(UnidadMedida entidad)
            {
                string Mensaje = "OK";
                bool OperacionExitosa = false;

                if (entidad.UnidadId > 0)
                {
                    OperacionExitosa = Actualizar(entidad);
                }
                else
                {
                    OperacionExitosa = Agregar(entidad);
                }

                if (!OperacionExitosa)
                {
                    Mensaje = "La información ingresada no es valida";
                }

                return Mensaje;
            }

            public UnidadMedida ObtenerPorId(long id)
            {
                UnidadMedida UnidadMedidaActual = new UnidadMedida();

                try
                {
                    UnidadMedidaActual = db.Set<UnidadMedida>().Where(x => x.UnidadId == id).FirstOrDefault();
                }
                catch (Exception)
                {
                }

                return UnidadMedidaActual;
            }

            public List<UnidadMedida> ObtenerListado()
            {
                List<UnidadMedida> UnidadMedidas = new List<UnidadMedida>();

                try
                {
                    UnidadMedidas = db.Set<UnidadMedida>().OrderByDescending(x => x.Fecha).ThenByDescending(x => x.UnidadId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return UnidadMedidas;
            }

            public List<UnidadMedida> Buscar(string Buscar)
            {
                List<UnidadMedida> UnidadMedidas = new List<UnidadMedida>();

                try
                {
                    UnidadMedidas = db.Set<UnidadMedida>().Where(x => x.Nombre.Contains(Buscar) || x.Descripcion.Contains(Buscar)).OrderByDescending(x => x.Fecha).ThenByDescending(x => x.UnidadId).Take(200).ToList();
                }
                catch (Exception)
                {
                }

                return UnidadMedidas;
            }

        #endregion

    }
}
