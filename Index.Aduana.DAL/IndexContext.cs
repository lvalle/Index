﻿using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.DAL
{
    public class IndexContext : DbContext
    {
        public DbSet<Permiso> Permisos { get; set; }

        public DbSet<Menu> Menus { get; set; }

        public DbSet<Rol> Roles { get; set; }

        public DbSet<RolPermiso> RolPermisos { get; set; }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<UsuarioRol> UsuarioRoles { get; set; }

        public DbSet<UsuarioCliente> UsuarioClientes { get; set; }
        
        public DbSet<TelefonoTipo> TelefonoTipos { get; set; }

        public DbSet<PersonaTipo> PersonaTipos { get; set; }

        public DbSet<Persona> Personas { get; set; }

        public DbSet<PersonaTelefono> PersonaTelefonos { get; set; }

        public DbSet<Cuenta> Cuentas { get; set; }

        public DbSet<Pais> Paises { get; set; }

        public DbSet<Moneda> Monedas { get; set; }

        public DbSet<Entities.Aduana> Aduanas { get; set; }

        public DbSet<UnidadMedida> UnidadMedidas { get; set; }

        public DbSet<Bodega> Bodegas { get; set; }

        public DbSet<Materia> Materias { get; set; }
        
        public DbSet<Producto> Productos { get; set; }

        public DbSet<ProductoMateria> ProductoMaterias { get; set; }
        
        public DbSet<Formula> Formulas { get; set; }

        public DbSet<FormulaDetalle> FormulaDetalles { get; set; }

        public DbSet<CuentaCliente> CuentaClientes { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FormulaDetalle>().HasRequired(p => p.Materia).WithMany().HasForeignKey(e => e.MateriaId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Materia>().HasRequired(p => p.Cliente).WithMany().HasForeignKey(e => e.ClienteId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Materia>().HasRequired(p => p.Cuenta).WithMany().HasForeignKey(e => e.CuentaId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Materia>().HasRequired(p => p.Unidad).WithMany().HasForeignKey(e => e.UnidadId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Producto>().HasRequired(p => p.Cliente).WithMany().HasForeignKey(e => e.ClienteId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Producto>().HasRequired(p => p.Cuenta).WithMany().HasForeignKey(e => e.CuentaId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Producto>().HasRequired(p => p.Unidad).WithMany().HasForeignKey(e => e.UnidadId).WillCascadeOnDelete(false);
        }
    }
}
