﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Aduana")]
    public class Aduana
    {
        [Key, Column("Aduana_Id")]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long AduanaId { get; set; }

        [Column("Pais_Id")]
        public long PaisId { get; set; }

        [ForeignKey("PaisId")]
        public Pais Pais { get; set; }

        [Required(ErrorMessage="El nombre de la aduana es requerido")]
        [StringLength(250)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "La dirección de la aduana es requerido")]
        [StringLength(500)]
        public string Direccion { get; set; }

        public DateTime Fecha { get; set; }

        public int Correlativo { get; set; }
    }
}
