﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Bodega")]
    public class Bodega
    {
        [Key,Column("Bodega_Id")]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long BodegaId { get; set; }

        [Required(ErrorMessage="El nombre de la bodega es requerido")]
        [StringLength(250)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "La direccion de la bodega es requerida")]
        [StringLength(500)]
        public string Direccion { get; set; }

        public DateTime Fecha { get; set; }

        public int Correlativo { get; set; }
    }
}
