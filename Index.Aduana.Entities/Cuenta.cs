﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Cuenta")]
    public class Cuenta
    {
        [Key,Column("Cuenta_Id")]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long CuentaId { get; set; }

        [Required(ErrorMessage = "El nombre de la cuenta es requerida")]
        [StringLength(200)]
        public string Nombre { get; set; }

        [StringLength(500)]
        public string Descripcion { get; set; }

        public DateTime Fecha { get; set; }
        
        public int Correlativo { get; set; }        
    }
}
