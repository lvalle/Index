﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Cuenta_Cliente")]
    public class CuentaCliente
    {
        [Key,Column("Cuenta_Id", Order = 0)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long CuentaId { get; set; }

        [ForeignKey("CuentaId")]
        public Cuenta Cuenta { get; set; }

        [Key, Column("Cliente_Id", Order = 1)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public Persona Cliente { get; set; }       
    }
}