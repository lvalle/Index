﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Formula")]
    public class Formula
    {
        [Key, Column("Formula_Id")]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long FormulaId { get; set; }

        [Column("Cliente_Id")]
        public long ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public Persona Cliente { get; set; }

        [Column("Producto_Id")]
        public long ProductoId { get; set; }

        [ForeignKey("ProductoId")]
        public Producto Producto { get; set; }

        public DateTime Fecha { get; set; }

        public int Correlativo { get; set; }

        public List<FormulaDetalle> Detalles { get; set; }
    }
}
