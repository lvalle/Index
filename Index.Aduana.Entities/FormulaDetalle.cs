﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Formula_Detalle")]
    public class FormulaDetalle
    {
        [Key, Column(name: "Detalle_Id", Order = 0)]
        public int DetalleId { get; set; }

        [Key, Column(name: "Formula_Id", Order = 1)]
        public long FormulaId { get; set; }

        [ForeignKey("FormulaId")]
        public Formula Formula { get; set; }

        [Column("Materia_Id")]
        public long MateriaId { get; set; }

        [ForeignKey("MateriaId")]
        public Materia Materia { get; set; }

        public decimal Cantidad { get; set; }

        [Column("Cantidad_Merma")]
        public decimal CantidadMerma { get; set; }       
    }
}
