﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Materia")]
    public class Materia
    {
        [Key, Column("Materia_Id")]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long MateriaId { get; set; }      

        [Column("Cliente_Id")]
        public long ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public Persona Cliente { get; set; }

        [Column("Cuenta_Id")]
        public long CuentaId { get; set; }

        [ForeignKey("CuentaId")]
        public Cuenta Cuenta { get; set; }

        [Column("Unidad_Id")]
        public long UnidadId { get; set; }

        [ForeignKey("UnidadId")]
        public UnidadMedida Unidad { get; set; }

        [Required(ErrorMessage = "El código de la materia prima es requerido")]
        [StringLength(50)]
        public string Codigo { get; set; }

        [Required(ErrorMessage = "El nombre de la materia prima es requerido")]
        [StringLength(200)]
        public string Nombre { get; set; }
               
        [StringLength(300)]
        public string Descripcion { get; set; }
                
        [StringLength(50)]
        public string Barra { get; set; }

        [StringLength(50)]
        public string Partida { get; set; }       

        public DateTime Fecha { get; set; }

        public int Correlativo { get; set; }  
    }
}
