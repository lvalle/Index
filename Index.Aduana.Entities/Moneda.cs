﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Moneda")]
    public class Moneda
    {
        [Key, Column("Moneda_Id")]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long MonedaId { get; set; }      

        [Required(ErrorMessage = "El nombre de la moneda es requerida")]
        [StringLength(200)]
        public string Nombre { get; set; }

        public DateTime Fecha { get; set; }

        public int Correlativo { get; set; }
    }
}
