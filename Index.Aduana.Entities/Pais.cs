﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Pais")]
    public class Pais
    {
        [Key, Column("Pais_Id")]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long PaisId { get; set; }

        [Column("Pais_Padre_Id")]
        public long? PaisPadreId { get; set; }

        [Required(ErrorMessage = "El nombre del país es requerido")]
        [StringLength(200)]
        public string Nombre { get; set; }       

        public DateTime Fecha { get; set; }

        public int Correlativo { get; set; }
    }
}
