﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Persona")]
    public class Persona
    {
        [Key, Column(name: "Persona_Id")]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long PersonaId { get; set; }

        [Column("Tipo_Id")]
        public int TipoId { get; set; }

        public PersonaTipo Tipo { get; set; }
        
        [StringLength(25)]
        public string Nit { get; set; }

        [Required(ErrorMessage = "El nombre de la persona es requerido")]
        [StringLength(250)]
        public string Nombre { get; set; }

        [StringLength(300)]
        public string Direccion { get; set; }

        [Column("Representante_Legal")]
        [StringLength(250)]
        public string RepresentanteLegal { get; set; }

        [StringLength(100)]
        public string Codigo { get; set; }

        [Column("Codigo_Importador")]
        [StringLength(100)]
        public string CodigoImportador { get; set; }

        [Column("Codigo_Exportador")]
        [StringLength(100)]
        public string CodigoExportador { get; set; }

        [Column("Resolucion_Calificacion")]
        [StringLength(100)]
        public string ResolucionCalificacion { get; set; }

        [Column("Regimen_Calificacion")]
        [StringLength(100)]
        public string RegimenCalificacion { get; set; }

        [Column("Periodo_Fiscal")]
        [StringLength(100)]
        public string PeriodoFiscal { get; set; }

        [Column("Fecha_Resolucion")]
        public DateTime? FechaResolucion { get; set; }

        [Column("Fecha_Vencimiento")]
        public DateTime? FechaVencimiento { get; set; }

        [StringLength(500)]
        public string Observaciones { get; set; }

        [Column("Local_Externo")]
        public bool LocalExterno { get; set; }

        public DateTime Fecha { get; set; }

        public int Correlativo { get; set; }

        public List<PersonaTelefono> Telefonos { get; set; }

        public List<CuentaCliente> Cuentas { get; set; }
    }
}
