﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Persona_Telefono")]
    public class PersonaTelefono
    {
        [Key, Column(name: "Telefono_Id", Order = 0)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int TelefonoId { get; set; }

        [Key, Column("Persona_Id", Order = 1)]
        public long PersonaId { get; set; }

        [ForeignKey("PersonaId")]
        public Persona Persona { get; set; }

        [Column("Tipo_Id")]
        public int TipoId { get; set; }

        [ForeignKey("TipoId")]
        public TelefonoTipo Tipo { get; set; }       

        [StringLength(15)]
        public string Numero { get; set; }
    }
}
