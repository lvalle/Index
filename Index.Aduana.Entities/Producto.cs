﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Producto")]
    public class Producto
    {
        [Key, Column("Producto_Id")]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long ProductoId { get; set; }

        [Column("Cliente_Id")]
        public long ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public Persona Cliente { get; set; }

        [Column("Cuenta_Id")]
        public long CuentaId { get; set; }

        [ForeignKey("CuentaId")]
        public Cuenta Cuenta { get; set; }

        [Column("Unidad_Id")]
        public long UnidadId { get; set; }

        [ForeignKey("UnidadId")]
        public UnidadMedida Unidad { get; set; }

        [Required(ErrorMessage = "El código del producto es requerido")]
        [StringLength(50)]
        public string Codigo { get; set; }

        [Required(ErrorMessage = "El nombre del producto es requerido")]
        [StringLength(200)]
        public string Nombre { get; set; }
               
        [StringLength(300)]
        public string Descripcion { get; set; }
                
        [StringLength(50)]
        public string Barra { get; set; }

        [StringLength(50)]
        public string Partida { get; set; }     

        public DateTime Fecha { get; set; }

        public int Correlativo { get; set; }
        
        public List<ProductoMateria> Materias { get; set; }
    }
}
