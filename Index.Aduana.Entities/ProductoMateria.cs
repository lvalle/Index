﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Producto_Materia")]
    public class ProductoMateria
    {
        [Key, Column(name: "Producto_Id", Order = 0)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long ProductoId { get; set; }       

        [Key, Column(name: "Materia_Id", Order = 1)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long MateriaId { get; set; }

        [ForeignKey("MateriaId")]
        public Materia Materia { get; set; }       
    }
}
