﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Telefono_Tipo")]
    public class TelefonoTipo
    {
        [Key, Column(name: "Tipo_Id")]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int TipoId { get; set; }

        [StringLength(200)]
        public string Nombre { get; set; }
    }
}
