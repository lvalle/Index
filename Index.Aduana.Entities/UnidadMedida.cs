﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Index.Aduana.Entities
{
    [Table("Unidad_Medida")]
    public class UnidadMedida
    {
        [Key,Column("Unidad_Id")]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long UnidadId { get; set; }

        [Required(ErrorMessage="El nombre de la unidad de medida es requerido")]
        [StringLength(150)]
        public string Nombre { get; set; }

        [StringLength(500)]
        public string Descripcion { get; set; }

        public DateTime Fecha { get; set; }

        public int Correlativo { get; set; }
    }
}
