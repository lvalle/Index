﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Index.Aduana.Entities
{
    [Table("Usuario_Cliente")]
    public class UsuarioCliente
    {
        [Key, Column(name: "Usuario_Id", Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long UsuarioId { get; set; }

        [ForeignKey("UsuarioId")]
        public Usuario Usuario { get; set; }

        [Key, Column(name: "Cliente_Id", Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public Persona Cliente { get; set; }
    }
}
