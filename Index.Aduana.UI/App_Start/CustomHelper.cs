﻿using Index.Aduana.BLL;
using Index.Aduana.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Index.Aduana.UI.App_Start
{
    public static class CustomHelper
    {
        public static void getUserName()
        {
            if (HttpContext.Current.Session["Nombre"] == null)
            {
                var Usuario = new UsuarioBL().ObtenerPorLogin(HttpContext.Current.User.Identity.Name);

                if (Usuario != null)
                {
                    HttpContext.Current.Session["Usuario"] = Usuario;
                    HttpContext.Current.Session["Nombre"] = Usuario.Nombre;
                }
            }
        }

        public static void getUserName(string User)
        {
            if (HttpContext.Current.Session["Nombre"] == null)
            {
                var Usuario = new UsuarioBL().ObtenerPorLogin(User);

                if (Usuario != null)
                {
                    HttpContext.Current.Session["Usuario"] = Usuario;
                    HttpContext.Current.Session["Nombre"] = Usuario.Nombre;
                }

            }
        }

        public static long getUserId()
        {
            long UserId = 0;

            if (HttpContext.Current.Session["Usuario"] == null)
            {
                var Usuario = new UsuarioBL().ObtenerPorLogin(HttpContext.Current.User.Identity.Name);

                if (Usuario != null)
                {
                    HttpContext.Current.Session["Usuario"] = Usuario;
                    HttpContext.Current.Session["Nombre"] = Usuario.Nombre;
                    UserId = Usuario.UsuarioId;
                }
            }
            else
            {
                var Usuario = (Usuario)HttpContext.Current.Session["Usuario"];

                if (Usuario != null)
                {
                    UserId = Usuario.UsuarioId;
                }
            }

            return UserId;
        }

        public static void setTitle(string Header, string SubHeader)
        {
            HttpContext.Current.Session["Encabezado"] = Header;
            HttpContext.Current.Session["SubEncabezado"] = SubHeader;
        }

        public static void setCliente(Persona Cliente)
        {
            HttpContext.Current.Session["Cliente"] = Cliente;           
        }

        public static long getClienteId()
        {
            long ClienteId = 0;

            if (HttpContext.Current.Session["Cliente"] != null)           
            {
                var Cliente = (Persona)HttpContext.Current.Session["Cliente"];

                if (Cliente != null)
                {
                    ClienteId = Cliente.PersonaId;
                }
            }

            return ClienteId;
        }

        public static Persona getCliente()
        {
             Persona Cliente = new Persona();

            if (HttpContext.Current.Session["Cliente"] != null)
            {
                Cliente = (Persona)HttpContext.Current.Session["Cliente"];
            }

            return Cliente;
        }

        public static bool Permiso(string Permiso)
        {
            return new RolBL().AutorizacionPermisoPorUsuario(HttpContext.Current.User.Identity.Name, Permiso);
        }
    }
}