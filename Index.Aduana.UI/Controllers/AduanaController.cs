﻿using Index.Aduana.BLL;
using Index.Aduana.Entities;
using Index.Aduana.UI.App_Start;
using Index.Aduana.UI.Models;
using Sistema.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;

namespace Index.Aduana.UI.Controllers
{
    [Authorize]
    [HandleError]
    public class AduanaController : Controller
    {
        #region Metodos Privados

            private void CargaControles()
            {
                var Paises = new PaisBL().ObtenerListado(false);

                ViewBag.Paises = new SelectList(Paises, "PaisId", "Nombre");           
            }

        #endregion

        // GET: Aduana
        [Permiso("Index.Aduana.Ver_Listado")]
        public ActionResult Index(int? page, string search)
        {
            CustomHelper.setTitle("Aduana", "Listado");

            List<Entities.Aduana> Aduanas = new List<Entities.Aduana>();

            try
            {
                if (!string.IsNullOrWhiteSpace(search) && search != null)
                {
                    Aduanas = new AduanaBL().Buscar(search).ToList();
                }
                else
                {
                    Aduanas = new AduanaBL().ObtenerListado().ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = string.Format("Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace);
                return View("~/Views/Shared/Error.cshtml");
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(Aduanas.ToPagedList(pageNumber, pageSize));
        }

        [Permiso("Index.Aduana.Crear")]
        public ActionResult Crear()
        {
            CustomHelper.setTitle("Aduana", "Nueva");           

            this.CargaControles();
            return View();
        }

        [Permiso("Index.Aduana.Crear")]
        [HttpPost]
        public ActionResult Crear(Entities.Aduana modelo)
        {          
            if (ModelState.IsValid)
            {
                string strMensaje = new AduanaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Aduana-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }

            }           
           
            this.CargaControles();
            return View(modelo);
        }

        [Permiso("Index.Aduana.Editar")]
        public ActionResult Editar(long id)
        {
            Entities.Aduana AduanaActual = new AduanaBL().ObtenerPorId(id);

            if (AduanaActual == null)
            {
                return HttpNotFound();
            }

            CustomHelper.setTitle("Aduana", "Editar");           

            this.CargaControles();
            return View(AduanaActual);
        }

        [Permiso("Index.Aduana.Editar")]
        [HttpPost]
        public ActionResult Editar(Entities.Aduana modelo)
        {
            if (ModelState.IsValid)
            {   
                string strMensaje = new AduanaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Aduana-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }

            }          

            this.CargaControles();
            return View(modelo);
        }
    }
}