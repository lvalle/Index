﻿using Index.Aduana.BLL;
using Index.Aduana.Entities;
using Index.Aduana.UI.App_Start;
using Index.Aduana.UI.Models;
using Sistema.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;

namespace Index.Aduana.UI.Controllers
{
    [Authorize]
    [HandleError]
    public class BodegaController : Controller
    {      

        // GET: Bodega
        [Permiso("Index.Bodega.Ver_Listado")]
        public ActionResult Index(int? page, string search)
        {
            CustomHelper.setTitle("Bodega", "Listado");

            List<Bodega> Bodegas = new List<Bodega>();

            try
            {
                if (!string.IsNullOrWhiteSpace(search) && search != null)
                {
                    Bodegas = new BodegaBL().Buscar(search).ToList();
                }
                else
                {
                    Bodegas = new BodegaBL().ObtenerListado().ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = string.Format("Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace);
                return View("~/Views/Shared/Error.cshtml");
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(Bodegas.ToPagedList(pageNumber, pageSize));
        }

        [Permiso("Index.Bodega.Crear")]
        public ActionResult Crear()
        {
            CustomHelper.setTitle("Bodega", "Nueva");           

            return View();
        }

        [Permiso("Index.Bodega.Crear")]
        [HttpPost]
        public ActionResult Crear(Bodega modelo)
        {          
            if (ModelState.IsValid)
            {
                string strMensaje = new BodegaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Bodega-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }

            }         

            return View(modelo);
        }

        [Permiso("Index.Bodega.Editar")]
        public ActionResult Editar(long id)
        {
            Bodega BodegaActual = new BodegaBL().ObtenerPorId(id);

            if (BodegaActual == null)
            {
                return HttpNotFound();
            }

            CustomHelper.setTitle("Bodega", "Editar");           
                       
            return View(BodegaActual);
        }

        [Permiso("Index.Bodega.Editar")]
        [HttpPost]
        public ActionResult Editar(Bodega modelo)
        {
            if (ModelState.IsValid)
            {
                string strMensaje = new BodegaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Bodega-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }

            }   
          
            return View(modelo);
        }
    }
}