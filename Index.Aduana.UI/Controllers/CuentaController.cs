﻿using Index.Aduana.BLL;
using Index.Aduana.Entities;
using Index.Aduana.UI.App_Start;
using Index.Aduana.UI.Models;
using Sistema.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;

namespace Index.Aduana.UI.Controllers
{
    [Authorize]
    [HandleError]
    public class CuentaController : Controller
    {      

        // GET: Cuenta
        [Permiso("Index.Cuenta.Ver_Listado")]
        public ActionResult Index(int? page, string search)
        {
            CustomHelper.setTitle("Cuenta", "Listado");

            List<Cuenta> Cuentas = new List<Cuenta>();

            try
            {
                if (!string.IsNullOrWhiteSpace(search) && search != null)
                {
                    Cuentas = new CuentaBL().Buscar(search).ToList();
                }
                else
                {
                    Cuentas = new CuentaBL().ObtenerListado().ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = string.Format("Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace);
                return View("~/Views/Shared/Error.cshtml");
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(Cuentas.ToPagedList(pageNumber, pageSize));
        }

        [Permiso("Index.Cuenta.Crear")]
        public ActionResult Crear()
        {
            CustomHelper.setTitle("Cuenta", "Nueva");           

            return View();
        }

        [Permiso("Index.Cuenta.Crear")]
        [HttpPost]
        public ActionResult Crear(Cuenta modelo)
        {          
            if (ModelState.IsValid)
            {
                string strMensaje = new CuentaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Cuenta-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }

            }         

            return View(modelo);
        }

        [Permiso("Index.Cuenta.Editar")]
        public ActionResult Editar(long id)
        {
            Cuenta CuentaActual = new CuentaBL().ObtenerPorId(id);

            if (CuentaActual == null)
            {
                return HttpNotFound();
            }

            CustomHelper.setTitle("Cuenta", "Editar");           
                       
            return View(CuentaActual);
        }

        [Permiso("Index.Cuenta.Editar")]
        [HttpPost]
        public ActionResult Editar(Cuenta modelo)
        {
            if (ModelState.IsValid)
            {
                string strMensaje = new CuentaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Cuenta-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }

            }   
          
            return View(modelo);
        }
    }
}