﻿using Index.Aduana.BLL;
using Index.Aduana.Entities;
using Index.Aduana.UI.App_Start;
using Index.Aduana.UI.Models;
using Sistema.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;

namespace Index.Aduana.UI.Controllers
{
    [Authorize]
    [HandleError]
    public class FormulaController : Controller
    {
        #region Metodos Privados

            private void CargaControles()
            {
                var Productos = new ProductoBL().ObtenerListado(false, CustomHelper.getClienteId());
                var Materias = new MateriaBL().ObtenerListado(CustomHelper.getClienteId()); 

                ViewBag.Productos = new SelectList(Productos, "ProductoId", "Nombre");
                ViewBag.Materias = new SelectList(Materias, "MateriaId", "Nombre");        
            }

        #endregion

        // GET: Formula
        [Permiso("Index.Formula.Ver_Listado")]
        public ActionResult Index(int? page, string search)
        {
            CustomHelper.setTitle("Formula", "Listado");

            List<Formula> Formulas = new List<Formula>();

            try
            {
                if (!string.IsNullOrWhiteSpace(search) && search != null)
                {
                    Formulas = new FormulaBL().Buscar(search, CustomHelper.getClienteId()).ToList();
                }
                else
                {
                    Formulas = new FormulaBL().ObtenerListado(CustomHelper.getClienteId()).ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = string.Format("Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace);
                return View("~/Views/Shared/Error.cshtml");
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(Formulas.ToPagedList(pageNumber, pageSize));
        }

        [Permiso("Index.Formula.Crear")]
        public ActionResult Crear()
        {
            CustomHelper.setTitle("Formula", "Nueva");           

            this.CargaControles();
            return View();
        }

        [Permiso("Index.Formula.Crear")]
        [HttpPost]
        public ActionResult Crear(Formula modelo, long[] materiaIds, decimal[] cantidadIds, decimal[] cantidadMermaIds)
        {
            if (materiaIds == null || materiaIds.Length == 0)
            {
                ModelState.AddModelError("", "Debe seleccionar al menos una materia prima");
            }

            if (ModelState.IsValid)
            {
                modelo.Detalles = new List<FormulaDetalle>();
                for (int i = 0; i < materiaIds.Length; i++)
                {
                    modelo.Detalles.Add(new FormulaDetalle() { MateriaId = materiaIds[i], Cantidad = cantidadIds[i], CantidadMerma = cantidadMermaIds[i] });
                }

                modelo.ClienteId = CustomHelper.getClienteId();
                string strMensaje = new FormulaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Formula-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }
            }

            ViewBag.materiaIds = materiaIds;
            ViewBag.cantidadIds = cantidadIds;
            ViewBag.cantidadMermaIds = cantidadMermaIds;

            this.CargaControles();
            return View(modelo);
        }

        [Permiso("Index.Formula.Editar")]
        public ActionResult Editar(long id)
        {
            Formula FormulaActual = new FormulaBL().ObtenerPorId(id, true);

            if (FormulaActual == null)
            {
                return HttpNotFound();
            }

            CustomHelper.setTitle("Formula", "Editar");

            if (FormulaActual.Detalles != null && FormulaActual.Detalles.Count() > 0)
            {
                ViewBag.materiaIds = FormulaActual.Detalles.Select(x => x.MateriaId);
                ViewBag.cantidadIds = FormulaActual.Detalles.Select(x => x.Cantidad);
                ViewBag.cantidadMermaIds = FormulaActual.Detalles.Select(x => x.CantidadMerma);
            }
            else
            {
                ViewBag.materiaIds = 0;
                ViewBag.cantidadIds = 0;
                ViewBag.cantidadMermaIds = 0;
            }

            this.CargaControles();
            return View(FormulaActual);
        }

        [Permiso("Index.Formula.Editar")]
        [HttpPost]
        public ActionResult Editar(Formula modelo, long[] materiaIds, decimal[] cantidadIds, decimal[] cantidadMermaIds)
        {
            if (materiaIds == null || materiaIds.Length == 0)
            {
                ModelState.AddModelError("", "Debe seleccionar al menos una materia prima");
            }

            if (ModelState.IsValid)
            {
                modelo.Detalles = new List<FormulaDetalle>();
                for (int i = 0; i < materiaIds.Length; i++)
                {
                    modelo.Detalles.Add(new FormulaDetalle() { MateriaId = materiaIds[i], Cantidad = cantidadIds[i], CantidadMerma = cantidadMermaIds[i] });
                }

                modelo.ClienteId = CustomHelper.getClienteId();
                string strMensaje = new FormulaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Formula-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }
            }

            ViewBag.materiaIds = materiaIds;
            ViewBag.cantidadIds = cantidadIds;
            ViewBag.cantidadMermaIds = cantidadMermaIds;

            this.CargaControles();
            return View(modelo);
        }

        [Permiso("Index.Formula.Detalle")]
        public ActionResult Detalle(long id)
        {
            Formula FormulaActual = new FormulaBL().ObtenerPorId(id, true);

            if (FormulaActual == null)
            {
                return HttpNotFound();
            }

            CustomHelper.setTitle("Formula", "Detalle");            
                       
            return View(FormulaActual);
        }
    }
}