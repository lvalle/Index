﻿using Index.Aduana.UI.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Index.Aduana.Entities;
using Index.Aduana.BLL;

namespace Index.Aduana.UI.Controllers
{
    [Authorize]
    [HandleError]
    public class InicioController : Controller
    {
        // GET: Inicio
        public ActionResult Dashboard()
        {
            CustomHelper.setTitle("Dashboard", "Inicio");
            return View();
        }

        public ActionResult Clientes(int? page, string search)
        {
            CustomHelper.setTitle("Clientes", "Listado");

            List<Persona> Clientes = new List<Persona>();

            try
            {
                if (!string.IsNullOrWhiteSpace(search) && search != null)
                {
                    //Clientes = new PersonaBL().Buscar(search, 2).ToList();
                }
                else
                {
                    Clientes = new PersonaBL().ClientesPorUsuarioId(CustomHelper.getUserId()).ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = string.Format("Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace);
                return View("~/Views/Shared/Error.cshtml");
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(Clientes.ToPagedList(pageNumber, pageSize));
        }
    }
}