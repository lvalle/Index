﻿using Index.Aduana.BLL;
using Index.Aduana.Entities;
using Index.Aduana.UI.App_Start;
using Index.Aduana.UI.Models;
using Sistema.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;

namespace Index.Aduana.UI.Controllers
{
    [Authorize]
    [HandleError]
    public class Materia_PrimaController : Controller
    {
        #region Metodos Privados

            private void CargaControles()
            {
                var Unidades = new UnidadBL().ObtenerListado();
                var Cuentas = new CuentaBL().ObtenerCuentaPorClienteId(CustomHelper.getClienteId());

                ViewBag.Unidades = new SelectList(Unidades, "UnidadId", "Nombre");
                ViewBag.Cuentas = new SelectList(Cuentas, "CuentaId", "Nombre");   
            }

        #endregion

        // GET: Materia_Prima
        [Permiso("Index.Materia_Prima.Ver_Listado")]
        public ActionResult Index(int? page, string search)
        {
            CustomHelper.setTitle("Materia Prima", "Listado");

            List<Materia> Materias = new List<Materia>();

            try
            {
                if (!string.IsNullOrWhiteSpace(search) && search != null)
                {
                    Materias = new MateriaBL().Buscar(search,CustomHelper.getClienteId()).ToList();
                }
                else
                {
                    Materias = new MateriaBL().ObtenerListado(CustomHelper.getClienteId()).ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = string.Format("Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace);
                return View("~/Views/Shared/Error.cshtml");
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(Materias.ToPagedList(pageNumber, pageSize));
        }

        [Permiso("Index.Materia_Prima.Crear")]
        public ActionResult Crear()
        {
            CustomHelper.setTitle("Materia Prima", "Nuevo");         

            this.CargaControles();
            return View();
        }

        [Permiso("Index.Materia_Prima.Crear")]
        [HttpPost]
        public ActionResult Crear(Materia modelo)
        {          
            if (ModelState.IsValid)
            {
                modelo.ClienteId = CustomHelper.getClienteId();
                string strMensaje = new MateriaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Materia_Prima-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }
            }
           
            this.CargaControles();
            return View(modelo);
        }

        [Permiso("Index.Materia_Prima.Editar")]
        public ActionResult Editar(long id)
        {
            Materia MateriaActual = new MateriaBL().ObtenerPorId(id);

            if (MateriaActual == null)
            {
                return HttpNotFound();
            }

            CustomHelper.setTitle("Materia Prima", "Editar");          

            this.CargaControles();
            return View(MateriaActual);
        }

        [Permiso("Index.Materia_Prima.Editar")]
        [HttpPost]
        public ActionResult Editar(Materia modelo)
        {
            if (ModelState.IsValid)
            {
                modelo.ClienteId = CustomHelper.getClienteId();
                string strMensaje = new MateriaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Materia_Prima-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }
            }          

            this.CargaControles();
            return View(modelo);
        }        
    }
}