﻿using Index.Aduana.BLL;
using Index.Aduana.Entities;
using Index.Aduana.UI.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace Index.Aduana.UI.Controllers
{
    [Authorize]
    [HandleError]
    public class PaisController : Controller
    {
        // GET: Pais
        [Permiso("Index.Pais.Ver_Listado")]
        public ActionResult Index(int? page, string search)
        {
            CustomHelper.setTitle("Pais", "Listado");

            List<Pais> Paises = new List<Pais>();

            try
            {
                if (!string.IsNullOrWhiteSpace(search) && search != null)
                {
                    Paises = new PaisBL().Buscar(search).ToList();
                }
                else
                {
                    Paises = new PaisBL().ObtenerListado().ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = string.Format("Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace);
                return View("~/Views/Shared/Error.cshtml");
            }

            ViewBag.Search = search;

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(Paises.ToPagedList(pageNumber, pageSize));
        }
    }
}