﻿using Index.Aduana.BLL;
using Index.Aduana.Entities;
using Index.Aduana.UI.App_Start;
using Index.Aduana.UI.Models;
using Sistema.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;

namespace Index.Aduana.UI.Controllers
{
    [Authorize]
    [HandleError]
    public class ProductoController : Controller
    {
        #region Metodos Privados

            private void CargaControles()
            {
                var Unidades = new UnidadBL().ObtenerListado();
                var Materias = new MateriaBL().ObtenerListado(CustomHelper.getClienteId());
                var Cuentas = new CuentaBL().ObtenerCuentaPorClienteId(CustomHelper.getClienteId());                
                
                ViewBag.Unidades = new SelectList(Unidades, "UnidadId", "Nombre");
                ViewBag.Materias = new SelectList(Materias, "MateriaId", "Nombre");
                ViewBag.Cuentas = new SelectList(Cuentas, "CuentaId", "Nombre");   
            }

        #endregion

        // GET: Producto
        [Permiso("Index.Producto.Ver_Listado")]
        public ActionResult Index(int? page, string search)
        {
            CustomHelper.setTitle("Producto", "Listado");

            List<Producto> Productos = new List<Producto>();

            try
            {
                if (!string.IsNullOrWhiteSpace(search) && search != null)
                {
                    Productos = new ProductoBL().Buscar(search, CustomHelper.getClienteId()).ToList();
                }
                else
                {
                    Productos = new ProductoBL().ObtenerListado(true, CustomHelper.getClienteId()).ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = string.Format("Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace);
                return View("~/Views/Shared/Error.cshtml");
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(Productos.ToPagedList(pageNumber, pageSize));
        }

        [Permiso("Index.Producto.Crear")]
        public ActionResult Crear()
        {
            CustomHelper.setTitle("Producto", "Nuevo");         

            this.CargaControles();
            return View();
        }

        [Permiso("Index.Producto.Crear")]
        [HttpPost]
        public ActionResult Crear(Producto modelo, long[] materiaIds)
        {
            if (materiaIds == null || materiaIds.Length == 0)
            {
                ModelState.AddModelError("", "Debe seleccionar al menos una materia prima");
            }

            if (ModelState.IsValid)
            {
                modelo.Materias = new List<ProductoMateria>();
                for (int i = 0; i < materiaIds.Length; i++)
                {
                    modelo.Materias.Add(new ProductoMateria() { MateriaId = materiaIds[i] });
                }

                modelo.ClienteId = CustomHelper.getClienteId();
                string strMensaje = new ProductoBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Producto-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }
            }

            ViewBag.materiaIds = materiaIds;

            this.CargaControles();
            return View(modelo);
        }

        [Permiso("Index.Producto.Editar")]
        public ActionResult Editar(long id)
        {
            Producto ProductoActual = new ProductoBL().ObtenerPorId(id, true);

            if (ProductoActual == null)
            {
                return HttpNotFound();
            }

            CustomHelper.setTitle("Producto", "Editar");

            if (ProductoActual.Materias != null && ProductoActual.Materias.Count() > 0)
            {
                ViewBag.materiaIds = ProductoActual.Materias.Select(x => x.MateriaId);            
            }
            else
            {
                ViewBag.materiaIds = 0;             
            }

            this.CargaControles();
            return View(ProductoActual);
        }

        [Permiso("Index.Producto.Editar")]
        [HttpPost]
        public ActionResult Editar(Producto modelo, long[] materiaIds)
        {
            if (materiaIds == null || materiaIds.Length == 0)
            {
                ModelState.AddModelError("", "Debe seleccionar al menos una materia prima");
            }

            if (ModelState.IsValid)
            {
                modelo.Materias = new List<ProductoMateria>();
                for (int i = 0; i < materiaIds.Length; i++)
                {
                    modelo.Materias.Add(new ProductoMateria() { MateriaId = materiaIds[i] });
                }

                modelo.ClienteId = CustomHelper.getClienteId();
                string strMensaje = new ProductoBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Producto-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }
            }

            ViewBag.materiaIds = materiaIds;

            this.CargaControles();
            return View(modelo);
        }

        [Permiso("Index.Producto.Detalle")]
        public ActionResult Detalle(long id)
        {
            Producto ProductoActual = new ProductoBL().ObtenerPorId(id, true);

            if (ProductoActual == null)
            {
                return HttpNotFound();
            }

            CustomHelper.setTitle("Producto", "Detalle");

            return View(ProductoActual);
        }
    }
}