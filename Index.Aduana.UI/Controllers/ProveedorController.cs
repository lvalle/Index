﻿using Index.Aduana.BLL;
using Index.Aduana.Entities;
using Index.Aduana.UI.App_Start;
using Index.Aduana.UI.Models;
using Sistema.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;

namespace Index.Aduana.UI.Controllers
{
    [Authorize]
    [HandleError]
    public class ProveedorController : Controller
    {
        #region Metodos Privados

            private void CargaControles()
            {                
                var TelefonoTipos = new TelefonoTipoBL().ObtenerListado();
            
                ViewBag.TelefonoTipos = new SelectList(TelefonoTipos, "TipoId", "Nombre");               
            }

        #endregion

        // GET: Proveedor
        [Permiso("Index.Proveedor.Ver_Listado")]
        public ActionResult Index(int? page, string search)
        {
            CustomHelper.setTitle("Proveedor", "Listado");

            List<Persona> Personas = new List<Persona>();

            try
            {
                if (!string.IsNullOrWhiteSpace(search) && search != null)
                {
                    Personas = new PersonaBL().Buscar(search, 2).ToList();
                }
                else
                {
                    Personas = new PersonaBL().ObtenerListado(2).ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = string.Format("Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace);
                return View("~/Views/Shared/Error.cshtml");
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(Personas.ToPagedList(pageNumber, pageSize));
        }

        [Permiso("Index.Proveedor.Crear")]
        public ActionResult Crear()
        {
            CustomHelper.setTitle("Proveedor", "Nuevo");

            string strAtributo = "checked='checked'";

            ViewBag.LocalSi = strAtributo;
            ViewBag.LocalNo = "";

            this.CargaControles();
            return View();
        }

        [Permiso("Index.Proveedor.Crear")]
        [HttpPost]
        public ActionResult Crear(Persona modelo, bool local, int[] tiposIds, string[] telefonosIds)
        {
            modelo.Telefonos = new List<PersonaTelefono>();
            if (tiposIds != null && tiposIds.Count() > 0)
            {
                for (int i = 0; i < tiposIds.Length; i++)
                {
                    modelo.Telefonos.Add(new PersonaTelefono() { TipoId = tiposIds[i], Numero = telefonosIds[i] });
                }
            }            

            if (ModelState.IsValid)
            {
                modelo.TipoId = 2;
                modelo.LocalExterno = local;
                string strMensaje = new PersonaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Proveedor-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }
            }

            string strAtributo = "checked='checked'";

            ViewBag.LocalSi = local == true ? strAtributo : "";
            ViewBag.LocalNo = local == false ? strAtributo : "";

            ViewBag.tiposIds = tiposIds;
            ViewBag.telefonosIds = telefonosIds;           

            this.CargaControles();
            return View(modelo);
        }

        [Permiso("Index.Proveedor.Editar")]
        public ActionResult Editar(long id)
        {
            Persona PersonaActual = new PersonaBL().ObtenerPorId(id, true);

            if (PersonaActual == null)
            {
                return HttpNotFound();
            }

            CustomHelper.setTitle("Proveedor", "Editar");

            string strAtributo = "checked='checked'";

            ViewBag.LocalSi = PersonaActual.LocalExterno == true ? strAtributo : "";
            ViewBag.LocalNo = PersonaActual.LocalExterno == false ? strAtributo : "";

            if (PersonaActual.Telefonos != null && PersonaActual.Telefonos.Count() > 0)
            {
                ViewBag.tiposIds = PersonaActual.Telefonos.Select(x => x.TipoId);
                ViewBag.telefonosIds = PersonaActual.Telefonos.Select(x => x.Numero);
            }
            else
            {
                ViewBag.tiposIds = 0;
                ViewBag.telefonosIds = 0;  
            }          

            this.CargaControles();
            return View(PersonaActual);
        }

        [Permiso("Index.Proveedor.Editar")]
        [HttpPost]
        public ActionResult Editar(Persona modelo, bool local, int[] tiposIds, string[] telefonosIds)
        {
            modelo.Telefonos = new List<PersonaTelefono>();
            if (tiposIds != null && tiposIds.Count() > 0)
            {
                for (int i = 0; i < tiposIds.Length; i++)
                {
                    modelo.Telefonos.Add(new PersonaTelefono() { TipoId = tiposIds[i], Numero = telefonosIds[i] });
                }
            }

            if (ModelState.IsValid)
            {
                modelo.TipoId = 2;
                modelo.LocalExterno = local;
                string strMensaje = new PersonaBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Persona-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }
            }

            string strAtributo = "checked='checked'";

            ViewBag.LocalSi = local == true ? strAtributo : "";
            ViewBag.LocalNo = local == false ? strAtributo : "";

            ViewBag.tiposIds = tiposIds;
            ViewBag.telefonosIds = telefonosIds;            

            this.CargaControles();
            return View(modelo);
        }       
    }
}