﻿using Index.Aduana.BLL;
using Index.Aduana.Entities;
using Index.Aduana.UI.App_Start;
using Index.Aduana.UI.Models;
using Sistema.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;

namespace Index.Aduana.UI.Controllers
{
    [Authorize]
    [HandleError]
    public class UnidadController : Controller
    {      

        // GET: Unidad
        [Permiso("Index.Unidad.Ver_Listado")]
        public ActionResult Index(int? page, string search)
        {
            CustomHelper.setTitle("Unidad de Medida", "Listado");

            List<UnidadMedida> Unidades = new List<UnidadMedida>();

            try
            {
                if (!string.IsNullOrWhiteSpace(search) && search != null)
                {
                    Unidades = new UnidadBL().Buscar(search).ToList();
                }
                else
                {
                    Unidades = new UnidadBL().ObtenerListado().ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = string.Format("Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace);
                return View("~/Views/Shared/Error.cshtml");
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(Unidades.ToPagedList(pageNumber, pageSize));
        }

        [Permiso("Index.Unidad.Crear")]
        public ActionResult Crear()
        {
            CustomHelper.setTitle("Unidad de Medida", "Nueva");           

            return View();
        }

        [Permiso("Index.Unidad.Crear")]
        [HttpPost]
        public ActionResult Crear(UnidadMedida modelo)
        {          
            if (ModelState.IsValid)
            {
                string strMensaje = new UnidadBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Unidad-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }

            }         

            return View(modelo);
        }

        [Permiso("Index.Unidad.Editar")]
        public ActionResult Editar(long id)
        {
            UnidadMedida UnidadActual = new UnidadBL().ObtenerPorId(id);

            if (UnidadActual == null)
            {
                return HttpNotFound();
            }

            CustomHelper.setTitle("Unidad de Medida", "Editar");           
                       
            return View(UnidadActual);
        }

        [Permiso("Index.Unidad.Editar")]
        [HttpPost]
        public ActionResult Editar(UnidadMedida modelo)
        {
            if (ModelState.IsValid)
            {
                string strMensaje = new UnidadBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Unidad-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }

            }   
          
            return View(modelo);
        }
    }
}