﻿using Index.Aduana.BLL;
using Index.Aduana.Entities;
using Index.Aduana.UI.App_Start;
using Index.Aduana.UI.Models;
using Sistema.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;

namespace Index.Aduana.UI.Controllers
{
    [Authorize]
    [HandleError]
    public class UsuarioController : Controller
    {
        #region Metodos Privados

            private void CargaControles()
            {
                var Roles = new RolBL().ObtenerListado();
                var Clientes = new PersonaBL().ObtenerListado(1, true);

                ViewBag.Roles = new SelectList(Roles, "RolId", "Nombre");
                ViewBag.Clientes = new SelectList(Clientes, "PersonaId", "Nombre");    
            }

        #endregion

        // GET: Usuario
        [Permiso("Index.Usuario.Ver_Listado")]
        public ActionResult Index(int? page)
        {
            CustomHelper.setTitle("Usuario", "Listado");

            List<Usuario> Usuarios = new List<Usuario>();

            try
            {
                Usuarios = new UsuarioBL().ObtenerListado().ToList();
            }
            catch (Exception ex)
            {
                ViewBag.Error = string.Format("Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace);
                return View("~/Views/Shared/Error.cshtml");
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(Usuarios.ToPagedList(pageNumber, pageSize));
        }

        [Permiso("Index.Usuario.Crear")]
        public ActionResult Crear()
        {
            CustomHelper.setTitle("Usuario", "Nuevo");

            string strAtributo = "checked='checked'";

            ViewBag.ActivoSi = strAtributo;
            ViewBag.ActivoNo = "";

            ViewBag.ReiniciarPasswordSi = "";
            ViewBag.ReiniciarPasswordNo = strAtributo;

            ViewBag.AutenticarSiteSi = "";
            ViewBag.AutenticarSiteNo = strAtributo;

            ViewBag.AutenticarAndroidSi = strAtributo;
            ViewBag.AutenticarAndroidNo = "";

            this.CargaControles();
            return View();
        }

        [Permiso("Index.Usuario.Crear")]
        [HttpPost]
        public ActionResult Crear(Usuario modelo, int[] rolesIds, long[] clientesIds, bool autenticarSite, bool autenticarAndroid, bool activo, bool reiniciarPassword)
        {
            if (rolesIds == null || rolesIds.Length == 0)
            {
                ModelState.AddModelError("", "Debe seleccionar al menos un rol");
            }

            if (ModelState.IsValid)
            {
                modelo.Roles = new List<UsuarioRol>();
                for (int i = 0; i < rolesIds.Length; i++)
                {
                    modelo.Roles.Add(new UsuarioRol() { RolId = rolesIds[i] });
                }

                modelo.Clientes = new List<UsuarioCliente>();
                if (clientesIds != null && clientesIds.Count() > 0)
                {
                    for (int i = 0; i < clientesIds.Length; i++)
                    {
                        modelo.Clientes.Add(new UsuarioCliente() { ClienteId = clientesIds[i] });
                    }                     
                }

                modelo.Activo = activo;
                modelo.ReiniciarPassword = reiniciarPassword;

                string strMensaje = new UsuarioBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Usuario-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }

            }

            string strAtributo = "checked='checked'";

            ViewBag.ActivoSi = activo == true ? strAtributo : "";
            ViewBag.ActivoNo = activo == false ? strAtributo : "";

            ViewBag.ReiniciarPasswordSi = reiniciarPassword == true ? strAtributo : "";
            ViewBag.ReiniciarPasswordNo = reiniciarPassword == false ? strAtributo : "";

            ViewBag.AutenticarSiteSi = autenticarSite == true ? strAtributo : "";
            ViewBag.AutenticarSiteNo = autenticarSite == false ? strAtributo : "";

            ViewBag.AutenticarAndroidSi = autenticarAndroid == true ? strAtributo : "";
            ViewBag.AutenticarAndroidNo = autenticarAndroid == false ? strAtributo : "";

            ViewBag.RolesIds = rolesIds;
            ViewBag.ClientesIds = clientesIds;   

            this.CargaControles();
            return View(modelo);
        }

        [Permiso("Index.Usuario.Editar")]
        public ActionResult Editar(long id)
        {
            Usuario UsuarioActual = new UsuarioBL().ObtenerPorId(id, true);

            if (UsuarioActual == null)
            {
                return HttpNotFound();
            }

            CustomHelper.setTitle("Usuario", "Editar");

            if (UsuarioActual.Roles != null && UsuarioActual.Roles.Count() > 0)
            {
                ViewBag.RolesIds = UsuarioActual.Roles.Select(x => x.RolId);
            }
            else
            {
                ViewBag.RolesIds = 0;
            }

            if (UsuarioActual.Clientes != null && UsuarioActual.Clientes.Count() > 0)
            {
                ViewBag.ClientesIds = UsuarioActual.Clientes.Select(x => x.ClienteId);
            }
            else
            {
                ViewBag.ClientesIds = 0;
            }           

            string strAtributo = "checked='checked'";

            ViewBag.ActivoSi = UsuarioActual.Activo == true ? strAtributo : "";
            ViewBag.ActivoNo = UsuarioActual.Activo == false ? strAtributo : "";

            ViewBag.ReiniciarPasswordSi = UsuarioActual.ReiniciarPassword == true ? strAtributo : "";
            ViewBag.ReiniciarPasswordNo = UsuarioActual.ReiniciarPassword == false ? strAtributo : "";

            ViewBag.AutenticarSiteSi = UsuarioActual.AutenticarSite == true ? strAtributo : "";
            ViewBag.AutenticarSiteNo = UsuarioActual.AutenticarSite == false ? strAtributo : "";

            ViewBag.AutenticarAndroidSi = UsuarioActual.AutenticarAndroid == true ? strAtributo : "";
            ViewBag.AutenticarAndroidNo = UsuarioActual.AutenticarAndroid == false ? strAtributo : "";

            this.CargaControles();
            return View(UsuarioActual);
        }

        [Permiso("Index.Usuario.Editar")]
        [HttpPost]
        public ActionResult Editar(Usuario modelo, int[] rolesIds, long[] clientesIds, bool autenticarSite, bool autenticarAndroid, bool activo, bool reiniciarPassword)
        {
            if (rolesIds == null || rolesIds.Length == 0)
            {
                ModelState.AddModelError("", "Debe seleccionar al menos un rol");
            }           

            if (ModelState.IsValid)
            {
                modelo.Roles = new List<UsuarioRol>();
                for (int i = 0; i < rolesIds.Length; i++)
                {
                    modelo.Roles.Add(new UsuarioRol() { RolId = rolesIds[i] });
                }

                modelo.Clientes = new List<UsuarioCliente>();
                if (clientesIds != null && clientesIds.Count() > 0)
                {
                    for (int i = 0; i < clientesIds.Length; i++)
                    {
                        modelo.Clientes.Add(new UsuarioCliente() { ClienteId = clientesIds[i] });
                    }
                }

                modelo.Activo = activo;
                modelo.ReiniciarPassword = reiniciarPassword;

                string strMensaje = new UsuarioBL().Guardar(modelo);

                if (strMensaje.Equals("OK"))
                {
                    TempData["Usuario-Success"] = strMensaje;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", strMensaje);
                }

            }

            string strAtributo = "checked='checked'";

            ViewBag.ActivoSi = activo == true ? strAtributo : "";
            ViewBag.ActivoNo = activo == false ? strAtributo : "";

            ViewBag.ReiniciarPasswordSi = reiniciarPassword == true ? strAtributo : "";
            ViewBag.ReiniciarPasswordNo = reiniciarPassword == false ? strAtributo : "";

            ViewBag.AutenticarSiteSi = autenticarSite == true ? strAtributo : "";
            ViewBag.AutenticarSiteNo = autenticarSite == false ? strAtributo : "";

            ViewBag.AutenticarAndroidSi = autenticarAndroid == true ? strAtributo : "";
            ViewBag.AutenticarAndroidNo = autenticarAndroid == false ? strAtributo : "";

            ViewBag.RolesIds = rolesIds;
            ViewBag.ClientesIds = clientesIds;     

            this.CargaControles();
            return View(modelo);
        }
    }
}